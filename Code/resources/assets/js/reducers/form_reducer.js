const formReducer = (state = {
  fields: {
    stand_id: '',
    company_name: '',
    company_address: '',
    company_website: '',
    company_logo: '',
    user_firstname: '',
    user_lastname: '',
    usercompany_email: '',
    usercompany_phone: '',
  },
  bookingSaved: false, 
}, { type, payload }) => {
  switch (type) {
    case 'EDIT_FIELD':
      return {
        ...state,
        fields: {
          ...state.fields,
          [payload.name]: payload.value,
        },
      };
    case 'SUBMIT_BOOKING_START':
      return state;
    case 'SUBMIT_BOOKING_SUCCESS':
      return {
        ...state,
        bookingSaved: true,
      };
    case 'SUBMIT_BOOKING_FAILED':
      return state;
    default:
      return state;
  }
};

export default formReducer;
