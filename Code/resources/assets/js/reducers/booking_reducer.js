import {
    FETCH_BOOKINGS,
    ADD_BOOKING,
    BOOKING_SHOW,
    DELETE_BOOKING,
    EDIT_BOOKING,
    UPDATE_BOOKING,
    FETCH_BOOKING_SUCCESS,
    EDIT_BOOKING_SUCCESS,
    BOOKING_SHOW_SUCCESS,
    UPDATE_BOOKING_SUCCESS,


    USER_INFO, USER_INFO_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    bookingsList: {bookings: [], error: null, loading: false},
    newBooking: {booking: null, error: null, loading: false},
    deletedBooking: {booking: null, error: null, loading: false},
    editBooking: {booking: null, error: null, loading: false},
    activeBooking: {booking: null, error: null, loading: false},
    updateBooking: {booking: null, error: null, loading: false},
};


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_BOOKINGS:
            return {...state, bookingsList: {bookings: [], error: null, loading: true}};
        case FETCH_BOOKING_SUCCESS:
            return {...state, bookingsList: {bookings: action.payload.data, error: null, loading: false}};
        case BOOKING_SHOW:
            return {...state, activeBooking: {booking: null, error: null, loading: true}};
        case BOOKING_SHOW_SUCCESS:
            return {...state, activeBooking: {booking: action.payload.data, error: null, loading: false}};
        case EDIT_BOOKING:
            return {...state, editBooking: {booking: null, error: null, loading: true}};
        case EDIT_BOOKING_SUCCESS:
            return {...state, editBooking: {booking: action.payload.data, error: null, loading: false}}
        case UPDATE_BOOKING:
            return {...state, updateBooking: {booking: null, error: null, loading: true}};
        case UPDATE_BOOKING_SUCCESS:
            return {...state, updateBooking: {booking: true, error: null, loading: false}};
        default:
            return state;
    }
}

