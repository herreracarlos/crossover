import { combineReducers } from 'redux';
import form from './form_reducer';
import map from './map_reducer';

const rootReducer = combineReducers({
   form,
   map,
   // auth: authReducer,
   // bookings: bookingReducer
});
export default rootReducer;

