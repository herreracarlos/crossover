const mapReducer = (state = {
  centres: [],
  loading: false,
  selectedCentreId: null,
}, { type, payload }) => {
  switch (type) {
    case 'MAP_UPDATE_START':
      return {
        ...state,
        loading: true,
      };
    case 'MAP_UPDATE_CENTRES':
      return {
        ...state,
        loading: false,
        centres: payload.centres,
      };
    case 'MAP_SELECT_CENTRE':
      return {
        ...state,
        selectedCentreId: payload.centreId,
      }
    default:
      return state;
  }
};

export default mapReducer;
