import axios from 'axios';
// import jwtdecode from 'jwt-decode';

const ROOT_URL = 'http://crossover.dev';




const createSubmitAction = () => (dispatch, getState) => {
  axios.post(
    `${ROOT_URL}/api/bookings`,
    getState().form.fields
  ).then(response => {
    dispatch({
      type:'SUBMIT_BOOKING_SUCCESS',
      payload: response
    });
  });

  return {
    type: 'SUBMIT_BOOKING_START',
  };
};
export { createSubmitAction };