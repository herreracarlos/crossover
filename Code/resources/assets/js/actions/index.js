import axios from 'axios';
import jwtdecode from 'jwt-decode';
import {browserHistory} from 'react-router';
import {
  AUTH_USER,
  AUTH_ERROR,
  LOGOUT_USER,
  FETCH_BOOKINGS,
  ADD_BOOKING,
  POST_SHOW,
  DELETE_BOOKING,
  EDIT_BOOKING,
  UPDATE_BOOKING,
  FETCH_BOOKING_SUCCESS,
  EDIT_BOOKING_SUCCESS,
  POST_SHOW_SUCCESS,
  UPDATE_BOOKING_SUCCESS,
  USER_INFO_SUCCESS,
  USER_INFO,
} from './types';

const ROOT_URL = 'http://crossover.dev';


// User and Auth actions
// 
export function loginUser({email,password}){
  return function(dispatch){
  axios.post(`${ROOT_URL}/api/login`,{email,password})
  .then(response => {
    dispatch({type: AUTH_USER,
    payload:response.data.token
    });
    localStorage.setItem('token',response.data.token);
  }).catch(()=>{
    dispatch(authError("Empty Required Field"));
  });
  }
}

export function userInfo(){
  return dispatch => {
    axios.get(`${ROOT_URL}/api/userinfo`,{
    headers:{authorization:`Bearer`+localStorage.getItem('token')}
    })
      .then(response =>{
        dispatch({
          type:USER_INFO_SUCCESS,
          payload:response
        })
      })
  }
}


export function registerUser({email,password}){
  return function(dispatch){
    axios.post(`${ROOT_URL}/api/register`,{email,password})
      .then(response =>{
      dispatch({type:AUTH_USER});
      localStorage.setItem('token',response.data.token);
      browserHistory.push('/posts');
      })
      .catch(response => dispatch(authError(response.data.error)));

  }
}

export function sendBooking({ stand_id,
                 name,
                 address,
                 website,
                 logo,
                 firstname,
                 lastname,
                 email,
                 phone})
{
  return function(dispatch){
  axios.post(`${ROOT_URL}/api/bookings`,{ stand_id,
      name,
      address,
      website,
      logo,
      firstname,
      lastname,
      email,
      phone},
    {
    headers:{authorization:localStorage.getItem('token')}
  })
  .then(response => {
    dispatch({
    type:ADD_BOOKING,
    payload: response
    })
  })
  }
}

export function fetchBookings(){
  return dispatch => {
  dispatch({
    type: FETCH_BOOKINGS
  });

  axios.get(`${ROOT_URL}/api/bookings`,{
   headers: { authorization: localStorage.getItem('token') }
  })
  .then(response => {
      dispatch(fetchBookingSuccess(response));
  })
  }
}

export function fetchBookingSuccess(posts){
  return {
  type:FETCH_BOOKING_SUCCESS,
  payload:posts
  };
}

export function bookingShowSuccess(post){
  return {
  type:POST_SHOW_SUCCESS,
  payload:post
  };
}

export function EditBooking(id){
  return dispatch => {
  dispatch({type:EDIT_BOOKING});

  axios.get(`${ROOT_URL}/api/booking/${id}/edit`,{
   headers: { authorization: localStorage.getItem('token') }
  })
  .then(response =>{
    dispatch(editBookingSuccess(response))
  })
  };
}

export function editBookingSuccess(bookings){
  return {
  type:EDIT_BOOKING_SUCCESS,
  payload:bookings
  };
}

export function updateBooking(id,{ stand_id,
  name,
  address,
  website,
  logo,
  firstname,
  lastname,
  email,
  phone}){
  return dispatch => {
  dispatch({type:UPDATE_BOOKING});

  axios.put(`${ROOT_URL}/api/bookings/${id}`,{ stand_id,
      name,
      address,
      website,
      logo,
      firstname,
      lastname,
      email,
      phone},
  {
    headers: { authorization: localStorage.getItem('token') }
  })
  .then(response => {
    dispatch(updateBookingSuccess(response));
  })
  };
}
export function updateBookingSuccess(post){
  return {
  type:UPDATE_BOOKING_SUCCESS,
  response:post
  }
}

export function deleteBooking(id){
  return function(dispatch){
  axios.delete(`${ROOT_URL}/api/bookings/${id}`,{
   headers: { authorization: localStorage.getItem('token') }
  })
  .then(response =>{
    dispatch({
      type:DELETE_BOOKING,
      payload:response
    });
  })
  }
}

export function authError(error){
  return {
    type:AUTH_ERROR,
    payload:error
  }
}

export function logoutUser() {
  localStorage.removeItem('token');
  return { type: LOGOUT_USER };
}
