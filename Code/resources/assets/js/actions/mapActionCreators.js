import axios from 'axios';
// import jwtdecode from 'jwt-decode';

const ROOT_URL = 'http://crossover.dev';




const createInitAction = () => (dispatch, getState) => {
  axios.get(
    `${ROOT_URL}/centres`
  ).then(response => {
    dispatch({
      type:'MAP_UPDATE_CENTRES',
      payload: {centres: response.data}
    });
  });

  return {
    type: 'MAP_UPDATE_START',
  };
};
export { createInitAction };