/**
 * Created by carlosherrera on 11/7/17.
 */
String.prototype.inflectorCamelize = function () {

    var x = this.replace(/[\s\W]/g, '_').split('_').map(function (e) {
        return e.charAt(0).toUpperCase() + e.substr(1)
    }).join('');
    return x;
};
String.prototype.inflectorClassify = function () {

    var x = this.replace(/]\[/g, '_').replace('[', '_').replace(']', '_').split('_').map(function (e) {
        return e.charAt(0).toUpperCase() + e.substr(1)
    }).join('');
    return x;
};
String.prototype.inflectorSnakefy = function () {
    var x = this.inflectorClassify().replace(/[A-Z]/g, function (x) {
        return '_' + x.toLowerCase();
    });
    if (x.charAt(0) == '_') {
        return x.substr(1);
    } else {
        return x;
    }

};

String.prototype.inflectorHumanize = function () {
    var x = this.inflectorCamelize().replace(/[A-Z]/g, function (x) {
        return ' ' + x;
    });
    return x.trim();

};

String.prototype.inflectorDotToName = function () {
    var x = this.split('.');
    if (x.length > 1) {
        let r = x.shift();
        x=x.map(function (e) {
            return '[' + e.inflectorSnakefy() + ']';
        });
        x.unshift(r);
    }

    return x.join('');

};


