import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
// import Header from './header';
import Root from './crossover/Root';
class App extends Component {
  render() {
    return (
	    <div >
            <div className="container">
        	     {/*<Header />*/}
        	     <Root/>
               {this.props.children}
           </div>
	</div>
    );
  }
}

const ConnectedApp = connect(
  state => ({
    bookings: state.bookings,
  }),
  dispatch => ({
    addBooking: (...args) => dispatch('aaa', ...args)
  })
)(App);

export default ConnectedApp;