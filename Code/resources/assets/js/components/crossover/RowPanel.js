import React, {Component} from 'react';
import {Col, Row, Panel} from 'react-bootstrap';


class RowPanel extends Component {

    render() {
        const header = this.props.h3 ? <h3>{this.props.h3}</h3> : this.props.header;
        return <Row {...this.props.row}>

            <Col {...this.props.col}>
                <Panel header={header}>

                    {this.props.children}

                </Panel>
            </Col>
        </Row>
    }
}

RowPanel.defaultProps = {
    row: {},
    col: {
        md: 8,
        mdOffset: 2,
    }

};

export default RowPanel;