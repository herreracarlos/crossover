/**
 * Created by carlosherrera on 18/7/17.
 */

import React, { Component} from 'react';
import PropTypes from 'prop-types';
import {createSubmitAction} from '../../actions/formActionCreators';
import * as actions from '../../actions/index';
import { connect } from 'react-redux';
// import {reduxForm} from 'redux-form';
// import {initialize} from 'redux-form';

import FormInput from "./FormInput";
import FormButton from "./FormButton";
// import rb, {FieldGroup} from 'react-bootstrap';
// console.log(rb, FieldGroup);
class AddBooking extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  handleFormSubmit(formProps) {
    this.props.addBooking(formProps);
    this.context.router.push('/bookings');
  }

  render() {
    const {
      handleSubmit,
      bookingSaved,
    } = this.props;



    return !bookingSaved && <div>
      <form onSubmit={ handleSubmit }>
        <input type="hidden" name="_token" value={this.props.token}/>
        <h2>Company Details</h2>
        <fieldset>
          <FormInput onChange={e => this.props.onChange(e) }name="stand_id" value={this.props.fields.stand_id } />
          <FormInput onChange={e => this.props.onChange(e) }name="company_name" value={this.props.fields.company_name } />
          <FormInput onChange={e => this.props.onChange(e) }name="company_address" value={this.props.fields.company_address } />
          <FormInput onChange={e => this.props.onChange(e) }name="company_website" value={this.props.fields.company_website } />
          <FormInput onChange={e => this.props.onChange(e) }name="company_logo" type="text" value={this.props.fields.company_logo } />
        </fieldset>
        <h3>Contact Details</h3>
        <fieldset>
          <FormInput onChange={e => this.props.onChange(e) } name="user_firstname" label="First Name"/>
          <FormInput onChange={e => this.props.onChange(e) } name="user_lastname" label="Last Name"/>
          <FormInput onChange={e => this.props.onChange(e) } name="usercompany_email" label="Email"/>
          <FormInput onChange={e => this.props.onChange(e) } name="usercompany_phone" label="Phone"/>
        </fieldset>
        <button id="AddBookingSubmit" className="btn-success" onClick={this.props.onSubmit }>Make Reservation</button>
      </form>
    </div>
  }
}


function validate(formProps){
  const errors = {};
  if (!formProps.stand_id){errors.stand_id="stand_id is required";}
  if (!formProps.name){errors.name="name is required";}
  if (!formProps.address){errors.address="address is required";}
  if (!formProps.website){errors.website="website is required";}
  if (!formProps.logo){errors.logo="logo is required";}
  if (!formProps.firstname){errors.firstname="firstname is required";}
  if (!formProps.lastname){errors.lastname="lastname is required";}
  if (!formProps.email){errors.email="email is required";}
  if (!formProps.phone){errors.phone="phone is required";}




  return errors;
}

function mapStateToProps(state){
  return {
    add:state.bookings.editPost,
    initialValues: state.bookings.editBooking.booking,
    updatePostStatus: state.bookings.updateBooking

  }
}
export default connect(
  state => ({
    fields: state.form.fields,
    bookingSaved: state.form.bookingSaved,
  }),
  dispatch => ({
    handleSubmit: (...args) => dispatch({type:'TEST', ...args}),
    addBooking: (...args) => dispatch({type:'TEST', ...args}),
    onChange: e => dispatch({type:'EDIT_FIELD', payload: {
        name: e.target.name,
        value: e.target.value
      }}),
    onSubmit: e => {
      e.preventDefault();
      dispatch(createSubmitAction());
    },
  })
)(AddBooking);





