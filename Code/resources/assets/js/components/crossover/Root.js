/**
 * Created by carlosherrera on 18/7/17.
 */
import React, {Component} from 'react';
import SearchOnMap from "./SearchOnMap";
import EventList from "./EventList";
import AddBooking from "./AddBooking";
import Panel from "./RowPanel";
import Menu from "./Menu";
import {Grid} from 'react-bootstrap';
import Confirmation from "./Confirmation";


class Root extends Component {
    render() {
        return <div>
            <Menu/>
            <Grid>
            <Panel h3="Search Availables fairs in your city"><SearchOnMap/></Panel>
            <Panel h3="Or maybe you want see what's next"><EventList/></Panel>
            <Panel h3="Your stand goes here"><AddBooking/></Panel>
            <Confirmation/>
            </Grid>
        </div>
    }
}

export default Root;