/**
 * Created by carlosherrera on 18/7/17.
 */
// import GoogleMapReact from 'google-maps-react';
import { connect } from 'react-redux';
import GoogleMapReact from 'google-map-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

const CentreBanner = ({title, children, onClick}) => <div
    style={{
      width: '20px',
      height: '20px',
      border: '2px solid #f00',
      background: '#fff',
      borderRadius: '10px',
      cursor:'pointer'
    }}
    onClick={ onClick }
  ></div>;


class SearchOnMap extends Component {
  static defaultProps = {
    center: {
      lat: -33.8688,
      lng: 151.2093,
    },
    zoom: 7
  };

  render() {
    const {
      center,
      zoom,
      centres,
      loading,
      onSelectCentre,
    } = this.props;

    return <div style={{height:'300px'}}><GoogleMapReact
      defaultCenter={ center }
      defaultZoom={ zoom }
      bootstrapURLKeys={ {key:'AIzaSyBk7nyqUV4MN8KoczVDECk14sZyqGh8LoE' }}
    >
      {
        loading
          ? <CentreBanner lat={center.lat} lng={center.lng}>Loading centres...</CentreBanner>
          : centres.map(({
          id,
          name,
          latitude,
          longitude
        }) => <CentreBanner key={id} lat={latitude} lng={longitude} onClick={
            e => {
              e.preventDefault();
              onSelectCentre(id);
            }
          }>
          {name}
        </CentreBanner>)
      }
    </GoogleMapReact></div>;

    // return <div>todo</div>
  }
}

SearchOnMap.propTypes = {
  centres: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  zoom: PropTypes.number,
  center: PropTypes.object,
};

export default connect(
  state => ({
    centres: state.map.centres,
    loading: state.map.loading,
  }),
  dispatch => ({
    onSelectCentre: id => {
      dispatch({
        type: 'MAP_SELECT_CENTRE',
        payload: {
          centreId: id,
        }
      })
    }
  })
)(SearchOnMap);
