/**
 * Created by carlosherrera on 18/7/17.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Jumbotron, Button} from 'react-bootstrap';
import { connect } from 'react-redux';


class Confirmation extends React.Component {
    render() {
        return this.props.bookingSaved && <Jumbotron>
            <h1>All Done!</h1>
                <p>Now you need to wait for our confirmation email</p>
                <p>And of course one of our agent will call you to solve
                    any question that you may have</p>
            <p><Button bsStyle="primary">Go to Top</Button></p>
        </Jumbotron>
    }
}

Confirmation.propTypes = {
    bookingSaved: PropTypes.bool.isRequired,
}

export default connect(
    state => ({
        bookingSaved: state.form.bookingSaved,
    })
)(Confirmation);
