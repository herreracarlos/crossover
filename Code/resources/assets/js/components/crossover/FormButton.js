/**
 * Created by carlosherrera on 18/7/17.
 */
import FormInput from './FormInput'
import React from 'react';

class FormButton extends FormInput {

    renderLabel() {
        return ''
    }

    renderInput() {
        const p = {...this.props, className: this.props.className + " btn"};
        var t = p.label;
        delete p.label;

        return <button {...p} onChange={this.handleChange}>{t}</button>

    }
}
FormButton.defaultProps = {type: 'submit', ...FormInput.defaultProps}
export default FormButton