/**
 * Created by carlosherrera on 18/7/17.
 */
import FormInput from './FormInput'
import React from 'react';

class FormSelect extends FormInput {

    renderInput() {
        console.log(this.props.options);
        const selectOptions = this.props.options.forEach((o, i) => (o + ' ' + i));
        return <select id={this.props.name} className="form-control" name={this.props.name}
                       value={this.state.value} required={this.props.required} onChange={this.handleChange}>
            {selectOptions}
        </select>

    }
}
FormSelect.defaultProps = {
    name: '',
    id: '',
    label: '',
    errors: [],
    required: false,
    options: {}
}
export default FormSelect