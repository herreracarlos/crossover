/**
 * Created by carlosherrera on 18/7/17.
 */
import { connect } from 'react-redux';
import React, {Component} from 'react';
import {Col, Row, Grid, Thumbnail, Button} from 'react-bootstrap';


class EventList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subpanelOpen: false,
    };
    this.openSubPanel = this.openSubPanel.bind(this);
  }
  render() {
    const {
      centres,
      selectedCentreId,
    } = this.props.map;
    const selectedCentre = selectedCentreId ?
      centres.find(d => d.id == selectedCentreId) : null;
    return <div>
      {
        selectedCentre === null
          ? <div>Select a centre in the map</div>
          : <div>
              <Grid>
                <Row>
                  <Col xs={6} md={4}>
                    <Thumbnail src={selectedCentre.logo} alt="242x200">
                      <h3>Thumbnail label</h3>
                      <p>{selectedCentre.description}</p>
                      <p>
                        <Button bsStyle="primary" onClick={e => {
                          e.preventDefault();
                          this.openSubPanel(!this.state.subpanelOpen);
                        }}>+ info</Button>
                      </p>
                    </Thumbnail>
                  </Col>

                </Row>
                {
                  this.state.subpanelOpen && <Row>
                    <h4>Venues</h4>
                    {
                      selectedCentre.venues.map((venue, idx) => {
                        <div key={idx}>
                          <div>Name: {venue.name}</div>
                        </div>
                      })
                    }
                  </Row>
                }
              </Grid>


          </div>
      }
    </div>;
  }
  openSubPanel(subpanelOpen = true) {
    this.setState({subpanelOpen});
  }
}

export default connect(
  state => ({
    map: state.map,
  })
)(EventList);
