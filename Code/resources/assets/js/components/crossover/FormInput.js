/**
 * Created by carlosherrera on 18/7/17.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';

class FormInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputErrors: props.inputErrors || [],
      value: props.value,
      name:props.name.inflectorDotToName(),
    };
    this.handleChange = this.handleChange.bind(this);
  }


  renderErrors() {
    let message = [];
    if (this.state.inputErrors.length) {
      console.log(this.state.inputErrors);
      message = this.state.inputErrors.map((e, i) => <span className="help-block" id={i}>{e}</span>);
    }
    return message;
  }

  handleChange(event) {
    // this.setState({value: event.target.value});
    this.props.onChange(event);
  }

  getIdentification() {
    const i=this.props.id || this.state.name.inflectorClassify();
    return i;
  }

  renderInput() {
    const p = {
      id: this.getIdentification(),
      ...this.props,
      name:this.state.name,
      className: this.props.className + " form-control",
      value: this.props.value,
    };
    if (p.label) {
      delete p.label;
    }
    return <input {...p} onChange={this.handleChange}/>

  }

  renderLabel() {
    const l = this.props.label ? this.props.label : this.props.name.inflectorHumanize();
    return <label htmlFor={this.getIdentification()} className="col-md-4 control-label">{l}</label>

  }

  render() {
    const renderLabel = this.renderLabel();
    const renderInput = this.renderInput();
    const renderErrors = this.renderErrors();
    return <div className={this.state.inputErrors.length ? 'form-group has-error' : 'form-group'}>
      {renderLabel}
      <div className="col-md-6">
        {renderInput}
        {renderErrors}
      </div>
    </div>
  }
}
FormInput.propTypes = {
  onChange: PropTypes.func.isRequired,
};
FormInput.defaultProps = {
  name: '',
  className:'',
  required: false,
  type: 'text'
};
export default FormInput