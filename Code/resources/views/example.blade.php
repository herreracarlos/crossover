@extends('layouts.app')

@section('content')
<MapCentres>
    <Centre>
        <Info>
            Name,Location,Etc
        </Info>
        <EventList>
            <Event>
                <Venue>
                    <VenueInfo></VenueInfo>
                    <VenueMap></VenueMap>
                </Venue>
                <StandList>
                    <Stand available="true">
                        <Standinfo></Standinfo>
                    </Stand>
                    <Stand available="true">
                        <Standinfo></Standinfo>
                        <Booking>
                            <form></form>
                        </Booking>
                    </Stand>
                    <Stand available="false">
                        <Standinfo></Standinfo>
                        <CompanyLogo></CompanyLogo>
                    </Stand>
                </StandList>
            </Event>
        </EventList>
    </Centre>
    <Centre>

    </Centre>
</MapCentres>
@endsection
