<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

foreach (['centres', 'venues', 'events', 'companies', 'users', 'bookings'] as $web_resource) {
    Route::resource($web_resource, Str::ucfirst(Str::camel($web_resource) . 'Controller'));
}
