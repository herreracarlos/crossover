<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Str;

Route::get('/', function () {
    return view('react');
});

Auth::routes();



foreach (['centres', 'venues', 'events', 'companies', 'users', 'bookings'] as $web_resource) {
    Route::resource($web_resource, Str::ucfirst(Str::camel($web_resource) . 'Controller'));
}