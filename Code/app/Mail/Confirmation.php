<?php
/**
 * Created by PhpStorm.
 * User: carlosherrera
 * Date: 22/7/17
 * Time: 3:16 PM
 */


namespace App\Mail;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Confirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Booking
     */
    protected $booking;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     *
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.confirmation')
            ->with([
                'orderName' => $this->booking->name,
                'orderPrice' => $this->booking->price,
            ]);
    }
}