<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Company;
use App\User;
use App\Stand;
use Illuminate\Http\Request;


class BookingsController extends SimpleController
{


    public function store(Request $request)
    {

        $stand = Stand::find($request->stand_id);
//        dump($stand);
        $company_data = [
            'name' => $request->company_name,
            'address' => $request->company_address,
            'website' => $request->company_website ?: '',
            'logo' => $request->company_logo ?: '',
            'email' => $request->usercompany_email,
            'phone' => $request->usercompany_phone,
            'contact' => "$request->user_firstname $request->user_lastname",
        ];
        $company = new Company($company_data);
        $company->save();

//        dump($company);
        $user_data = [
            'firstname' => $request->user_firstname,
            'lastname' => $request->user_lastname,
            'email' => $request->usercompany_email,
            'phone' => $request->usercompany_phone,
            'password' => bcrypt(str_random(10)),
        ];
//        dump($user_data);

        $user = new User($user_data);
        $user->company()->associate($company);
        $user->save();

//        dump($user);

        $booking = new Booking();
        $booking->stand()->associate($stand);
        $booking->company()->associate($company);
        $booking->user()->associate($user);
        $booking->save();
        return $booking;
    }


}