<?php

namespace App\Http\Controllers;

use App\Centre;
use Illuminate\Http\Request;

class CentresController extends SimpleController
{

    public function index()
    {
        return Centre::with('venues','venues.events')->get();

    }
}