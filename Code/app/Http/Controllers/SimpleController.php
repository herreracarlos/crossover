<?php
/**
 * Created by PhpStorm.
 * User: carlosherrera
 * Date: 17/7/17
 * Time: 9:56 PM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;


class SimpleController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    var $Model;

    function __construct()
    {
        $this->Model = $this->model();

    }

    private function model()
    {
        $class = Str::singular(str_ireplace('Controller', '', class_basename($this)));
        return "App\\$class";
    }

    public function index()
    {
        $model=$this->Model;
        return $model::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model=$this->Model;
        return $model::findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model=$this->Model;
        return $model::findOrFail($id)->delete();
    }




}