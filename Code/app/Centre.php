<?php

namespace App;


class Centre extends SimpleModel
{



    public function venues()
    {

        return $this->hasMany(Venue::class, 'centre_id');
    }
}
