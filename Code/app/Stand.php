<?php

namespace App;


use Illuminate\Http\Request;


class Stand extends SimpleModel
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
