<?php

namespace App;


class Venue extends SimpleModel
{
    //
    protected $visible=['events'];

    public function events() {
        return $this->hasMany(Event::class);
    }
}
