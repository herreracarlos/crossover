<?php

namespace App;



class Company extends SimpleModel
{
    //

    protected $fillable = [
        'name',
        'address',
        'website',
        'logo',
        'email',
        'phone',
        'contact'


    ];


    public function users()
    {
        return $this->hasMany(User::class);
    }


}
