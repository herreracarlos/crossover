<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Company;
use App\User;

class BookingTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {

        $this->browse(function (Browser $browser) {
            $fakeCompany = factory(Company::class)->make();
            $fakeUser = factory(User::class)->make(['company_id'=>$fakeCompany->id]);
            $browser->visit('/')
                ->waitForText('Company Name')
                ->type('stand_id',1)
                ->type('company_name',$fakeCompany->name)
                ->type('company_address',$fakeCompany->address)
                ->type('company_website',$fakeCompany->website)
                ->type('company_logo',$fakeCompany->logo)
                ->type('#UserFirstname',$fakeUser->firstname)
                ->type('#UserLastname',$fakeUser->lastname)
                ->type('#UsercompanyEmail',$fakeCompany->email)
                ->type('#UsercompanyPhone',$fakeCompany->phone)
                ->pause(5000)
                ->pressAndWaitFor('Make Reservation')
                ->pause(5000)
                ->assertSee('All Done')

            ;


        });
    }
}
