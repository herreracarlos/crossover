<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        },
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Centre::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->companyEmail,
        'latitude'=>function() {
                return rand(12819428,33852596) / 1000000 * -1;
        },
//        'latitude' => $faker->numberBetween(-33.852511,151.173486),
//        'latitude' => $faker->numberBetween(-33.852596,151.286371),
//        'latitude' => $faker->numberBetween(-33.945456,151.149297),BL
//        'latitude' => $faker->numberBetween(-33.948243,151.266549),
        'longitude' => function() {
            return mt_rand(114000000,152000000) / 1000000;
        },
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->citySuffix,
        'zip' => $faker->postcode,
        'country' => $faker->country,
        'website' => "http://www.{$faker->domainName}.com/",
        'phone' => $faker->phoneNumber,
        'description' => $faker->paragraph,
        'logo' => $faker->imageUrl(242,200),
    ];
});

$factory->define(App\Venue::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'centre_id' => function () {
            return factory('App\Centre')->create()->id;
        },
        'capacity' => $faker->numberBetween(2000, 4000),
        'sqm' => $faker->numberBetween(500, 4000),
        'location' => $faker->sentence,
        'description' => $faker->paragraph,
        'logo' => $faker->imageUrl(242,200),
    ];
});
$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'website' => $faker->domainName,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->companyEmail,
        'contact' => $faker->firstName . " " . $faker->lastName,
        'logo'=>$faker->imageUrl(128,128)
    ];
});
$factory->define(App\Event::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'venue_id' => function () {
            return factory(App\Venue::class)->create()->id;
        },
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        },
        'capacity' => $faker->numberBetween(2000, 4000),
        'sqm' => $faker->numberBetween(500, 4000),
        'description' => $faker->paragraph,
    ];
});
$factory->define(App\Stand::class, function (Faker\Generator $faker) {
    return [
        'venue_id' => function () {
            return factory(App\Venue::class)->create()->id;
        },
        'event_id' => function () {
            return factory(App\Event::class)->create()->id;
        },
        'price' => $faker->numberBetween(500, 20000),
        'area' => '',
        'confirmed_at' => $faker->dateTime,
    ];
});
$factory->define(App\Booking::class, function (Faker\Generator $faker) {
    return [
        'stand_id' => function () {
            return factory(App\Stand::class)->create()->id;
        },
        'selected' => true,
        'company_id' => function () {
            return factory(App\User::class)->create()->company_id;
        }, 'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },

        'confirmed_at' => $faker->dateTime,
    ];
});