<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $centres = factory(App\Centre::class, 1)->create();

        $centres->each(function ($centre) {
            $venues = factory(App\Venue::class, 2)->create(['centre_id' => $centre->id]);
            $venues->each(function ($venue) {

                $companies = factory(App\Company::class, 1)->create();
                $companies->each(function ($company) {
                    factory(App\User::class, 2)->create(['company_id' => $company->id]);
                });

                $events = factory(App\Event::class, 5)->create(['company_id' => $companies->first()->id, 'venue_id' => $venue->id]);
                $events->each(function ($event) {
                    factory(App\Stand::class, 10)->create(['event_id' => $event->id]);
                });
            });
        });
    }
}
